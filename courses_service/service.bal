import ballerina/http;

listener http:Listener coursesEP = new(9090);

service /courses on coursesEP {

    resource function get getAllCourses() returns Course[]|error? {
        return courses.toArray();
    }

    resource function post addCourse(@http:Payload Course value) returns string | error? {
        string[] conflictingIds = [];
        foreach Course course in courses{
            if(course.code == value.code){
                conflictingIds.push(value.code);
            }
        }
        if (conflictingIds.length() > 0) {
            return string: 'join(" ", "Conflicting course codes:  ", ...conflictingIds);
        }else{
            courses.add(value);
            return "Course added successfully";
        }
    }

    resource function get getCourseByCode/[string code]() returns Course|error? {
        Course? course = courses[code];

        if course is (){
            return error("No course returned");
        }
        return course;
    }
}

public type Course record{|
    readonly string code;
    string name;
    int credits;
    string[] topics;
|};

public final table<Course> key(code) courses = table [
    {code:"DSA621S", name:"Distributed Systems", credits:15, topics:["Interprocess-communication", "Remote-invocation"]}
];