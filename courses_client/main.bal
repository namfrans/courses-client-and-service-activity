import ballerina/io;
import ballerina/http;

http:Client ep = check new ("http://localhost:9090");

public function main() {
    io:println("About to make an http request...");

        json | error? get_all_course_response = ep -> get("/courses/getAllCourses");
        io:println("\nAll course in table: \n", get_all_course_response);

        string|error? add_course_response = ep -> post("/courses/addCourse", {code:"SYD611S", name:"Sustainable development", credits:15, topics:["Development design and sustainable", "Sustainability vs SD"]});
        io:println("\nPOST message:",add_course_response);

        json | error? get_user_by_code = ep -> get("/courses/getCourseByCode/DSA621S");
        io:println("\nReturned course: \n", get_user_by_code);
}
