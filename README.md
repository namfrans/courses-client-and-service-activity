# Courses client and service activity

## TODO
- [X] Get all courses from table
- [X] Create/Add courses to the table
- [X] Get courses by ID
- [ ] Make POST to allow a batch of courses to be created concurently

## STEPS
1. Fork
2. Pull code from fr-branch
3. Create your own branch
4. When done coding, commit to your branch.
5. Make a merge request
6. Verifying and merging branches with main branch.
